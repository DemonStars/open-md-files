#### Стиль кода

##### Именование файлов

---
##### Классы
Классы именуются по правилам СamelCase.

Для классов расширяющих  android компоненты, имена классов должны заканчиваться названием этого компонента.

Пример:
```
 SignInActivity, SignInFragment, ImageUploaderService, ChangePasswordDialog, MyCustomView
```

Абстрактные класс и интерфейсы.



| Type          | Good              | Bad           |
|:--------------|:------------------|:--------------|
| abstract class| Human / BaseHuman	| AbsHuman      |
| interface	    | HumanBehavior     | IHumanBehavior|

---
##### Именование компонентов архитектуры 

| Компонент       | Имя класса                 |
| :---------------|:---------------------------|
| UseCase         | DoSomeWorkCase             |
| Mapper          | SomeViewModelToModelMapper |

##### Файлы ресурсов
Файлы ресурсов именуются по правилам lower_case через землю.

---
##### Drawable файлы
| Asset Type  |	Prefix	     |Example                  |
|:----------- |:-------------|:------------------------|
| Action bar  | ab_	         | ab_stacked.9.png        |
| Button	  | btn_	     | btn_send_pressed.9.png  |
| Dialog	  | dialog_      | dialog_top.9.png        |
| Divider     | divider_     | divider_horizontal.9.png|
| Icon	      | ic_	         | ic_star.png             |
| Menu	      | menu_	     | menu_submenu_bg.9.png   |
| Notification| notification_| notification_bg.9.png   |
| Tabs	      | tab_	     | tab_pressed.9.png       |

Именование иконок (взято из [Android iconography guidelines](https://material.io/guidelines/style/icons.html)):

| Asset Type	                 | Префикс	         | Пример                  |
| :------------------------------|:------------------|:------------------------|
| Icons	                         | ic_	             | ic_star.png             |
| Launcher icons                 | ic_launcher	     | ic_launcher_calendar.png|
| Menu icons and Action Bar icons| ic_menu	         | ic_menu_archive.png     |
| Status bar  icons	             | ic_stat_notify	 | ic_stat_notify_msg.png  |
| Tab icons                      | ic_tab	         | ic_tab_recent.png       |
| Dialog icons	                 | ic_dialog         | ic_dialog_info.png      |

Именование состояний селекторов.

| Состояние| Суффикс	| Пример                  |
|:---------|:-----------|:------------------------|
| Normal   | _normal	| btn_order_normal.9.png  |
| Pressed  | _pressed	| btn_order_pressed.9.png |
| Focused  | _focused	| btn_order_focused.9.png |
| Disabled | _disabled	| btn_order_disabled.9.png|
| Selected | _selected	| btn_order_selected.9.png|


---
#####  Файлы разметки
Имена файлов разметки должны совпадать с названием android компонентов которым служат,
имя этого компонента должно быть в начале имени файла разметки.

Например, мы создаем разметку для  SignInActivity, разметка будет называться: activity_sign_in.xml.

| Компонент       | Имя класса          | Имя файла разметки             |
| :---------------|:--------------------|:-------------------------------|
| Activity	      | UserProfileActivity	| activity_user_profile.xml      |
| Fragment	      | SignUpFragment      | fragment_sign_up.xml           |
| Dialog	      | ChangePasswordDialog| view_dialog_change_password.xml|
| AdapterView item|	---                 | li_person.xml                  |
| Partial layout  |	---	                | view_stats_bar.xml             |

---
##### Идентификаторы элементов разметки
Активные элементы разметки должны иметь уникальный id.
Активными элементами являются: TextView с изменяемым из кода текстом, цветом и т.п., EditText, Button, ImageView, кликабельные View или ViewGroup, корневой элемент.

Идентификаторы именуются: <имя файла разметки><тип элемента><особенность элемента>

Типы элементов можно сокращать общепринятыми сокращениями, например `_imageview_` → `_iv_`
Корневые элементы ViewGroup можно в качестве типа элемента писать `root`, `content` или `container`.

Хорошо:
`fragment_refill_new_card_iv_logo`
`fragment_freemium_fin_btn_show_legal_agreement`
`li_timeline_account_info_root`

Плохо:
`pager_card_root` - не понятно что за layout
`fragment_deposit_error_result_btn` - сначала btn, потом result 

---
#####  Файлы меню
Так же как и файлы разметки, файлы меню должны совпадать с именем компонетов.

Например, если мы определяем меню которое используем в UserActivity, то имя файла меню будет activity_user.xml

Хорошим тоном считается не включать в имя файла меню слово "menu", потому, что этот компонент уже находится в каталоге menu.

---
#####  Файлы values
Файлы values именуются во множественном числе:

string**s**.xml

style**s**.xml

color**s**.xml

dimen**s**.xml

attr**s**.xml

Если сущность содержит в себе очень много значений однотипных ресурсов - лучше вынести их в отдельный файл с ресурсами.

Например:
Есть экран создания платежа, на котором присутствует большое число Spinner'ов, каждый из них заполняется множеством строковых значений.
В таком случае, лучше будет создать отдельный файл res/values/strings_payment.xml

---
#### Стиль кода

Проект написан на языке Kotlin.

[Документация](https://kotlinlang.org/docs/kotlin-docs.pdf)

[Онлайн задачи для обучения](https://try.kotlinlang.org/#/Kotlin%20Koans/Introduction/Hello,%20world!/Task.kt)

---
#####  Именование переменных

Переменные должны определяться в верху файла и подчиняться следующим правилам:

Приватные, не статические поля начинаются с префикса m. (напр.: mToolbar)
Приватные статические переменные начинаются с префикса s. (напр.: sInstance)
Другие переменные именуются в нижнем регистре.
Константы именуются в верхнем регистре через землю. (напр.: ALL_CAPS_WITH_UNDERSCORES, если значение 
строковое, то оно(значение) обозначается в нижнем регистре через землю напр.: tag_map_fragment)

Пример:
```
class MyClass {

        var publicField = 1
        private val sSingleton = MyClass()
        private var mPrivate = 1

        companion object {
            const val SOME_CONST = 43
            val SOME_CONSTANT = 42
	    }
}
```

---
#####  Именование аббревиатур

| Хорошо        | Плохо         |
| :-------------|:--------------|
| XmlHttpRequest| XMLHTTPRequest|
| getCustomerId	| getCustomerID |
| String url    | String URL    |
| long id	    | long ID       |


---

#####  Использование отспупов
Мы используем стандартное форматирование среды.

Максимальная длина строки 120 символов

Если условие простое и умещается в стандартную длину строки, то фигурные скобки можно не использовать:

```
// хорошо
if (condition) body()

// плохо
if (condition)
body()
```

Простые условия с else лучше оформлять через when:
```
// хорошо
when (condition) {
	true -> success()
	false -> error()
}
 ```

 ```
// плохо
if (condition) success() else error()
```

Data классы и конструкторы классов с несколькими параметрами
```
class DestinationViewModel(

        val ownerName: String,
        val ownerId: Long

) : SearchListItemViewModel {

    override fun onItemClicked(view: View) {

    }
}
```

Классы с одной переменной не умещающиеся в длину строки
```
class DepositSaveAndValidateReqData(

        @Transient private val mReqModel: DepositSaveAndValidateReqModel

) : BaseReqData(Data(mReqModel)) {}
```

Методы
```
private fun getLimitsPairViewModel(limitsPairModel: CardLimitsPairModel)
        : CardLimitsPairViewModel {

    return mLimitsPairViewModelMapper.transform(limitsPairModel)
}
```

Rx цепочки
```
private fun getCardLimits() {
    mView.showProgressDialog()

    val items = ArrayList<CardLimitsPairViewModel>()

    mGetCardLimitsCase = GetCardLimitsFromApiCase(mCardId)
    mGetCardLimitsCase!!.execute(
            ApiSubscriberImpl<CardLimitsPairModel>(
                    onNext = {
 						val viewModel = getLimitsPairViewModel(it)items.add(viewModel)
                    },
                    onError = { onGetLimitsError(it) },
                    onCompleted = { addLimitsToFragment(items) }
 		)
    )
}
```

---
#####  Аннотации

Когда аннотация применима к классу, методу или конструтору они представлены после блока документации, по одной аннотации в строке
```
/* Блок документации которой нет */
@AnnotationA
@AnnotationB
public class MyAnnotatedClass { }
```
---
#####  Поля

Аннотации применимые к полям должны находиться на одной линии с объявлением переменной, до максимальной длины строки.
```
@Nullable @Mock val mDataManager : DataManager? = null
```
---
#####  Логирование

Для логирования представлен класс LogHelper.

Использовать нужно только его:
```
LogHelper.i("connectChat")
//12-20 14:40:06.522 21434-21434/ru.zhuck.webapp.stage I/Tochka_2_log: connectChat
```
Toast

Использовать класс ToastHelper:
```
 ToastHelper.showDebugToast("Test message")
```

---
#####  Порядок расположения членов класса

Константы
Поля
Абстрактные методы
Конструторы
Методы
Inner классы или интерфейсы


Пример:
```
public class MainActivity : Activity {

    private var mTitle : String? = null
    private var mTextViewTitle: TextView? = null

	companion object {

    fun newInstance(paymentId: Long): FavoritesFragment {
        val frag = FavoritesFragment()
        frag.mPaymentId = paymentId
        return frag
    	}
	}

    override fun onCreate() {
        ...
    }

    override fun setUpView() {
        ...
    }

    inner class AnInnerClass {
		...
    }
    
    override fun onDestroy(){
       ...
    }
}
```

Методы должны идти по мере их использования один за другим, чтобы код можно было читать как книгу без скроллинга вверх.

Пример:
```

 class PaymentCreatingFragment : BaseFragment() {

    override fun getLayoutId(): Int {
        return R.layout.fragment_payment_creating
    }

    override fun initViews(bundle: Bundle?) {
        initText()

        initActions()
    }

    private fun initText() {
        fragment_payment_creating_text.text = getTitleText()
    }

    private fun initActions(){
	    doSomeStuff()
	}

	private fun doSomeStuff(){
	    ...
	}
}
```

private методы желательно располагать сразу за местом первого использования, но после блока объявления переменных.

Пример:
```
class ClaimPassportViewModel {

    // ...
    val taxIdChangedListener = object : EtTextChangedListener {

        override fun onTextChanged(text: String) {

            initSubmitButtonState()
        }
    }

    val someOtherVal = "123"
    // ... vars, vals

    private fun initSubmitButtonState() = condition && otherCondition // за первым использованием, но в блоке методов
}

```
---
#####  Однострочные функции

Если функция превышает 120 символов то не стоит её писать в одну строку.

Вот так плохо
```
private fun checkPermissionGranted() =
    AppState.appContext!!.checkSelfPermission(ACCESS_FINE_LOCATION) == PERMISSION_GRANTED

```
Вот так хорошо
```
 private fun checkPermissionGranted() {
     return AppState.appContext!!.checkSelfPermission(ACCESS_FINE_LOCATION) == PERMISSION_GRANTED
}
```

---
##### Комментирование

В идеале, код должен быть самодокументируемым, т.е. понятен без комментариев.
```
// Проверяет корректность taxId
private fun checkState(): Boolean = TaxIdValidator().isValidTaxId(taxId.get()) // плохо, т.к. не понятно

private fun isTaxIdValid(): Boolean = TaxIdValidator().isValidTaxId(taxId.get()) // хорошо, понятно без комментария
```

Следует комментировать когда:

из сигнатуры не понятен весь функционал метода
метод имеет несколько ветвей выполнения (например, в зависимости от переданного флага разный результат)
есть неявные ограничения
это интерфейс, т.к. должен быть определен контракт и для определения функциональности требуется смотреть реализации

```
нужен комментарий, не явный функционал
/**
 * сохраняет файл локально и возвращает локальный путь, не тот, который был в intent
 */
fun parseFileFromIntent(intent: Intent?)


нужен комментарий, ветви
/**
 * если флаг compressImages, то формат возвращаемого файла будет jpg 
 */
fun parseFileFromIntent(intent, Intent?, compressImages: Boolean) 


//не нужен комментарий, т.к. из контекста понятно, что maxSizeMb > 0
fun setMaxSize(maxSizeMb: Int) 


//не понятно, position с 0 или 1, лучше обозначить
fun getItemByPosition(position: Int)


// не понятны ограничения на offset, если имеются; если нет - комментарий можно опустить
fun setOffset(offset: Int) 

interface FilePickHelper { // нужен комментарий, т.к. интерфейс

    /** Получает файл по intent.data, сохраняет в папке приложения и возвращает локальный File.
     *
     * @param intent интент для получения данных
     * @param compressImages сжимает изображения (уменьшает размер, форматирует в jpg)
     * @param maxSizeMb ограничение на размер файлов,
     *          вызывает onError(FilePickHelperException) с сообщением при привышении
     *          null, если нет ограничений
     *
     * @return Single, выполняющий действия в onSubscribe. Поток по умолчанию не обозначен.
     */
    fun parseFileFromIntent(intent: Intent?,
                            compressImages: Boolean = true,
                            maxSizeMb: Int? = null): Single<File>
}

```

---
##### Импорты
В настройках IDE (Settings - Editor - Code style - Kotlin) должно быть указано:
- Top-level Symbols :: Use import with '\*' when at least **5** names used
- Java Statics and Enum Members :: Use import with '\*' when at least **3** names used
- Packages to Use Import with '\*':
  - `kotlinx.android.synthetic.*` with subpackages
  - более никаких

Также желательно настроить Auto import (Settings - Editor - General - Auto Import):
- [вкл] Add unambiguous imports on the fly
- Exclude from Import and Completion
  - `*.R.id.*` scope IDE (чтобы не импортил id вместо synthetic)

С автоимпортами надо быть аккуратным, т.к. если билд не прошел до конца, то optimize может удалить, например, synthetic.

---
##### @Keep vs Serializable
- Классы, которые содержат поля, помеченные аннотацией `@SerializedName` (например, ReqModel на слое data), помечаются аннотацией `@Keep`, вместо интерфейса `Serializable`.
```
@Keep
class SomeReqModel(

        @SerializedName("customer_code")
        val customerCode: String?

)
```
